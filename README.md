Dependencies:
* C++11 compiler
  * Default uses clang, change clang++ to g++ in Makefile if you use GCC

Testing bytecodes from previous assignments are in test3a/test3b/test3c folders and can be run automatically with testX.sh.

Wrapping script for FMLBench is gcfml.sh. Since I failed my bytecode compiler assignments, I have to use reference FML as bytecode compiler.
You will have to modify variables at the top, REAL_FML and REHAKDOR_FML, to point at the two binaries (reference and mine) on your system.
Then copy gcfml.sh to FMLBench and use it with run_benchmarks.sh.

This assignment was done by modifying my bytecode interpreter. Reference counting system was removed and replaced by mark-and-sweep garbage collector.
GC runs whenever allocation fails, if it fails again after GC, the program exits.
Existing runtime objects (RTOs) are kept in a linked list.
When marking phase happens, RTOs point at from roots are marked, and they mark connected RTOs and so on.
The linked list is then traversed and non-marked RTOs eliminated.
Marking from RTOs to RTOs is done using a polymorphic method declared in base class from which all RTOs derive.

A special root called TmpRoot is used for preventing temporary variables in VM's code from being deallocated.
Each time a bytecode instruction is finished being interpreted, TmpRoot is cleared.

Heap is managed by my own simple memory allocator.
Free space areas are described in a linked list sorted by starting address.
Allocations take into account necessary alignment etc. Deallocations coalesce neighbouring smaller free spaces into larger ones.
All RTOs are allocated through this memory allocator.
Standard C++ containers used in RTOs also use this memory allocator, so their memory usage counts as VM's heap usage.
