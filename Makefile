.PHONY: all clean deps
.SUFFICES:

CXX = clang++
CXXFLAGS ?= -O3 -Wall -pedantic -g
CXXFLAGS_BASE := -std=c++11 -fno-strict-aliasing
LDFLAGS ?=
LDFLAGS_BASE :=

SRC := constant_pool heap operand_stack code_vector globals labels frame_stack tmp_root vm main
OBJ := $(addsuffix .o,$(SRC))
D := $(addprefix deps/,$(addsuffix .d,$(SRC)))
EXE := main

all: $(EXE)
clean:
	-rm $(EXE) $(OBJ) $(D)
deps/%.d: %.cpp
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_BASE) -MM $< -o $@
deps: $(D)

-include $(D)

%.o: %.cpp Makefile
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_BASE) -c $< -o $@
$(EXE): $(OBJ)
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_BASE) $^ $(LDFLAGS_BASE) $(LDFLAGS) -o $@
