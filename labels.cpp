#include "labels.hpp"

#include "types.hpp"
#include "exception.hpp"
#include "util.hpp"

#include <map>

void Labels::addLabel(fml_cp_index name, fml_ip address){
    if(!labels.emplace(name, address).second)
        throw InvalidBytecode("Existing label already registered");
}
fml_ip Labels::getLabel(fml_cp_index name){
    auto it = labels.find(name);
    MY_ASSERT(it != labels.end());
    return it->second;
}
