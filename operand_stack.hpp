#ifndef OPERAND_STACK_H
#define OPERAND_STACK_H

#include "heap.hpp"
#include "types.hpp"

#include <vector>

class OperandStack{
protected:
    std::vector<RTO *> stack;

public:
    ~OperandStack(void);
    void cleanup(void);

    RTO& pop(void);
    void push(RTO& pointer);
    RTO& peek(size_t index);
    size_t size(void);

    void mark(void);
};

#endif
