#include "heap.hpp"

#include "globals.hpp"
#include "operand_stack.hpp"
#include "frame_stack.hpp"
#include "tmp_root.hpp"
#include "util.hpp"
#include "types.hpp"
#include "exception.hpp"

#include <map>
#include <cstring>
#include <fstream>

#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

RTO::RTO(void)
: marked(false){
}
void RTO::clearMark(void){
    marked = false;
}
void RTO::mark(void){
    if(marked)
        return;
    marked = true;
    markChildren();
}
bool RTO::isMarked(void) const{
    return marked;
}
void RTO::markChildren(void){
}

RTO::Type RTONull::getType(void) const{
    return Type::Null;
}
RTOBoolean::RTOBoolean(bool pvalue)
: value(pvalue){
}
RTO::Type RTOBoolean::getType(void) const{
    return Type::Boolean;
}
RTOInteger::RTOInteger(fml_int pvalue)
: value(pvalue){
}
RTO::Type RTOInteger::getType(void) const{
    return Type::Integer;
}

RTOObject::RTOObject(void)
: parent(nullptr){
}
RTO::Type RTOObject::getType(void) const{
    return Type::Object;
}
void RTOObject::setParent(RTO& pparent){
    parent = &pparent;
}
void RTOObject::setMethod(fml_cp_index name, fml_cp_index method){
    methods.emplace(name, method);
}
void RTOObject::setField(fml_cp_index name, RTO& value){
    auto it = fields.find(name);
    if(it != fields.end()){
        it->second = &value;
    }else
        fields.emplace(name, &value);
}
RTO& RTOObject::getField(fml_cp_index name) const{
    auto it = fields.find(name);
    if(it == fields.end())
        throw InvalidBytecode("Attempted to load unknown field");
    return *it->second;
}
void RTOObject::markChildren(void){
    if(parent) // we may be in process of RTOObject creation
        parent->mark();
    for(auto f : fields)
        f.second->mark();
}

RTOArray::RTOArray(size_t size, RTO& filler)
: data(size, &filler){
}
RTO::Type RTOArray::getType(void) const{
    return Type::Array;
}
void RTOArray::setField(size_t index, RTO& value){
    data[index] = &value;
}
RTO& RTOArray::getField(size_t index) const{
    return *data[index];
}
void RTOArray::markChildren(void){
    for(auto d : data)
        d->mark();
}



Heap::Heap(size_t pheap_size, std::ofstream& plog_file, Globals& pglobals, OperandStack& poperand_stack, FrameStack& pframe_stack, TmpRoot& ptmp_root)
: heap_size(pheap_size), memory_used(0), allocations_count(0), log_file(plog_file),
  globals(pglobals), operand_stack(poperand_stack), frame_stack(pframe_stack), tmp_root(ptmp_root){
    heap_memory = std::unique_ptr<char[]>(new char[heap_size]);
    free_list.emplace_back(heap_memory.get(), heap_size);
    logEvent('S');
}
Heap::~Heap(void){
    cleanup();
}
void Heap::cleanup(void){
    for(auto rto : rto_list)
        freeRTO(rto);
    rto_list.clear();
}
uint64_t Heap::getTimestamp(void) const{
    struct timespec timespec;
    (void) clock_gettime(CLOCK_REALTIME, &timespec);
    return (uint64_t)timespec.tv_sec * 1000000000 + (uint64_t)timespec.tv_nsec;
}
void Heap::logEvent(char type){
    if(log_file.is_open())
        log_file << getTimestamp() << "," << type << "," << memory_used << std::endl;
}
size_t Heap::computeOffset(const void *ptr, size_t alignment) const{
    uintptr_t iptr = reinterpret_cast<uintptr_t>(ptr);
    return ((iptr + alignment - 1) & ~((uintptr_t)alignment - 1)) - iptr;
}
#define HEADER_SIZE (sizeof(uint8_t) + sizeof(uint16_t))
void *Heap::realAlloc(size_t size, size_t alignment){
    size_t minimum_size = HEADER_SIZE + size;
    MY_ASSERT(minimum_size <= UINT16_MAX); // maximum allocation size supported
    if(heap_size - memory_used < minimum_size) // heap is too full
        return nullptr;
    for(auto it = free_list.begin(); it != free_list.end(); ++it){
        if(it->size < minimum_size) // this will filter out most
            continue;
        uint8_t offset = computeOffset((char *)it->address + HEADER_SIZE, alignment) + HEADER_SIZE;
        uint16_t total_size = offset + size;
        if(it->size < total_size)
            continue;
        void *result = (char *)it->address + offset;
        //memset(it->address, /*0xCC*/0, total_size); // memory management bug detector: if something is freed too soon, this will cause crash
        *((uint8_t *)result - 1) = offset;
        memcpy((char *)it->address, &total_size, sizeof(uint16_t)); // may not be aligned; memcpy will be optimized away
        if(it->size > total_size){
            void *new_address = (char *)it->address + total_size;
            size_t new_size = it->size - total_size;
            insertFreeChunk(new_address, new_size, false);
        }
        free_list.erase(it);
        memory_used += total_size;
        if(++allocations_count == 100000){
            logEvent('A');
            allocations_count = 0;
        }
        return result;
    }
    return nullptr;
}
void Heap::insertFreeChunk(void *address, size_t size, bool coalesce){
    auto it = free_list.begin();
    for(; it != free_list.end(); ++it){
        if(it->address > address)
            break;
    }
    free_list.emplace(it, address, size);
    if(coalesce){
        --it; // it is now pointing at new item
        // coalesce in front
        auto tested = it;
        while(tested != free_list.begin()){
            --tested;
            if((char *)tested->address + tested->size != it->address)
                break;
            it->address = tested->address;
            it->size += tested->size;
            free_list.erase(tested);
            tested = it;
        }
        // coalesce behind
        tested = it;
        tested++;
        while(tested != free_list.end()){
            if((char *)it->address + it->size != tested->address)
                break;
            it->size += tested->size;
            free_list.erase(tested);
            tested = it;
            tested++;
        }
    }
}
void Heap::freeRTO(RTO *ptr){
    ptr->~RTO();
    free(ptr);
}
void Heap::garbageCollect(void){
    globals.mark();
    operand_stack.mark();
    frame_stack.mark();
    tmp_root.mark();
    auto to_be_removed = rto_list.end();
    for(auto it = rto_list.begin(); it != rto_list.end(); ++it){
        if(to_be_removed != rto_list.end()){
            rto_list.erase(to_be_removed);
            to_be_removed = rto_list.end();
        }
        RTO *rto = *it;
        if(rto->isMarked())
            rto->clearMark();
        else{
            freeRTO(rto);
            to_be_removed = it;
        }
    }
    if(to_be_removed != rto_list.end())
        rto_list.erase(to_be_removed);
    logEvent('G');
}
void *Heap::alloc(size_t size, size_t alignment){
    void *result;
    if((result = realAlloc(size, alignment)))
        return result;
    garbageCollect();
    if((result = realAlloc(size, alignment)))
        return result;
    // no recovery possible
    std::cerr << "Ran out of heap memory" << std::endl;
    throw std::bad_alloc();
}
void Heap::free(void *ptr){
    uint8_t offset = *((uint8_t *)ptr - 1);
    void *real_beginning = (char *)ptr - offset;
    uint16_t total_size;
    memcpy(&total_size, real_beginning, sizeof(uint16_t)); // may not be aligned; memcpy will be optimized away
    //memset(real_beginning, /*0xAA*/0, total_size); // memory management bug detector: if something is freed too soon, this will cause crash
    insertFreeChunk(real_beginning, total_size, true);
    memory_used -= total_size;
}
void Heap::registerRTO(RTO& rto){
    rto_list.insert(&rto);
}


Heap *HeapStdAllocatorState::heap = nullptr;
template<class T>
T *HeapStdAllocator<T>::allocate(size_t n){
    return static_cast<T *>(heap->alloc(n * sizeof(T), alignof(T)));
}
template<class T>
void HeapStdAllocator<T>::deallocate(T *p, size_t n) noexcept{
    heap->free(p);
}


template<class T, class... Ts>
T& HeapRTOAllocator::universalNew(Ts&&... args){
    T *result = static_cast<T *>(heap.alloc(sizeof(T), alignof(T)));
    new (result) T(std::forward<Ts>(args)...);
    heap.registerRTO(*result); // registering must be after constructor call, or map<> etc. could trigger GC and overwrite resulting RTO
    //heap.logEvent('A');
    return *result;
}
HeapRTOAllocator::HeapRTOAllocator(Heap& pheap)
: heap(pheap){
}
RTOBoolean& HeapRTOAllocator::newBoolean(bool value){
    return universalNew<RTOBoolean>(value);
}
RTOInteger& HeapRTOAllocator::newInteger(fml_int value){
    return universalNew<RTOInteger>(value);
}
RTOObject& HeapRTOAllocator::newObject(void){
    return universalNew<RTOObject>();
}
RTOArray& HeapRTOAllocator::newArray(fml_int value, RTO& filler){
    return universalNew<RTOArray>(value, filler);
}
