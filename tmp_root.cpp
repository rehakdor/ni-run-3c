#include "tmp_root.hpp"

#include "heap.hpp"

#include <set>

TmpRoot::~TmpRoot(void){
    cleanup();
}
void TmpRoot::cleanup(void){
    temporaries.clear();
}

void TmpRoot::insert(RTO& rto){
    temporaries.insert(&rto);
}

void TmpRoot::mark(void){
    for(auto t : temporaries)
        t->mark();
}
