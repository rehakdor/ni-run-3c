#include "frame_stack.hpp"

#include "types.hpp"
#include "util.hpp"

#include <vector>

void Frame::mark(void){
    for(auto it : locals)
        it->mark();
}

FrameStack::~FrameStack(void){
    cleanup();
}
void FrameStack::cleanup(void){
    while(!frames.empty())
        removeFrame();
}


void FrameStack::addFrame(Frame& frame){
    frames.push_back(&frame);
}
void FrameStack::removeFrame(void){
    MY_ASSERT(frames.size() > 0);
    Frame *frame = frames.back();
    frames.pop_back();
    delete frame;
}
Frame& FrameStack::getFrame(void){
    return *frames.back();
}
const Frame& FrameStack::getFrame(void) const{
    return *frames.back();
}

fml_ip FrameStack::getReturnAddress(void) const{
    return getFrame().return_address;
}
RTO& FrameStack::getLocal(fml_local_index index) const{
    RTO *result = getFrame().locals[index];
    MY_ASSERT(result);
    return *result;
}
void FrameStack::setLocal(fml_local_index local_index, RTO& rto){
    Frame& curr_frame = getFrame();
    curr_frame.locals[local_index] = &rto;
}

void FrameStack::mark(void){
    for(auto it : frames)
        it->mark();
}
